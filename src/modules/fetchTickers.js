import axios from "axios";

// constants
const FETCH_TICKERS_SUCCESS = 'FETCH_TICKERS_SUCCESS';
const FETCH_TICKERS_LOADING = 'FETCH_TICKERS_LOADING';
const FETCH_TICKERS_FAILURE = 'FETCH_TICKERS_FAILURE';

const CHANGE_ACTIVE_TICKER = 'CHANGE_ACTIVE_TICKER';

const SORT_TICKERS_BY_OPEN = 'SORT_TICKERS_BY_OPEN';
const SORT_TICKERS_BY_HIGH = 'SORT_TICKERS_BY_HIGH';
const SORT_TICKERS_BY_LOW = 'SORT_TICKERS_BY_LOW';
const SORT_TICKERS_BY_VOLUME = 'SORT_TICKERS_BY_VOLUME';
const SORT_TICKERS_BY_CLOSE = 'SORT_TICKERS_BY_CLOSE';

const RESET_TICKERS_SORT = 'RESET_TICKERS_SORT';

const initialState = {
    initItems: [],
    items: [],
    isLoading: false,
    isLoaded: false,
    hasError: false,
    sortedBy: '',
    active: ''
};

//action creators
export const tickersAreLoading = bool => {
    return {
        type: FETCH_TICKERS_LOADING,
        isLoading: bool
    };
};

export const tickersHaveError = bool => {
    return {
        type: FETCH_TICKERS_FAILURE,
        hasError: bool
    };
};

export const itemsFetchDataSuccess = tickers => {
    return {
        type: FETCH_TICKERS_SUCCESS,
        tickers
    };
};

export const changeTicker = ticker => {
    return {
        type: CHANGE_ACTIVE_TICKER,
        payload: ticker
    };
};

export const sortItems = criteria => {
    return {
        type: `SORT_TICKERS_BY_${criteria.toUpperCase()}`,
        payload: criteria
    }
};
export const resetSort = () => {
    return{
        type: RESET_TICKERS_SORT,
    };
};

//async action using thunk to dispatch functions, that return action objects, instead of dispatching objects
export const tickersFetchData = () => {
    let url = 'https://api.iextrading.com/1.0/stock/aapl/chart';
    return (dispatch) => {
        dispatch(tickersAreLoading(true));
        axios.get(url)
            .then((response) => {
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                dispatch(tickersAreLoading(false));
                dispatch(itemsFetchDataSuccess(response.data))
            })
            .catch(() => dispatch(tickersHaveError(true)));
    };
};

// REDUCER
export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TICKERS_FAILURE:
            return {
                ...state,
                error: action.hasError,
            };
        case FETCH_TICKERS_LOADING:
            return {
                ...state,
                isLoading: action.isLoading,
            };
        case FETCH_TICKERS_SUCCESS:
            return {
                ...state,
                isLoaded: true,
                initItems: action.tickers,
                items:  action.tickers,
                active: action.tickers[0]
            };
        case CHANGE_ACTIVE_TICKER:
            return{
                ...state,
                active: action.payload,
            };
        case SORT_TICKERS_BY_OPEN:
            return{
                ...state,
                items: [...state.items].sort((a,b) => (a.open - b.open)),
                sorted: action.payload
            };
        case SORT_TICKERS_BY_HIGH:
            return{
                ...state,
                items: [...state.items].sort((a,b) => (a.high - b.high)),
                sorted: action.payload
            };
        case SORT_TICKERS_BY_LOW:
            return{
                ...state,
                items: [...state.items].sort((a,b) => (a.low - b.low)),
                sorted: action.payload
            };
        case SORT_TICKERS_BY_VOLUME:
            return{
                ...state,
                items: [...state.items].sort((a,b) => (a.volume - b.volume)),
                sorted: action.payload
            };
        case SORT_TICKERS_BY_CLOSE:
            return{
                ...state,
                items: [...state.items].sort((a,b) => (a.close - b.close)),
                sorted: action.payload
            };
        case RESET_TICKERS_SORT:
            return{
                ...state,
                items: [...state.initItems],
                sorted: ''
        };
        default:
            return state;
    }
};
// SELECTORS
// export const getTickers = state => state.items;
