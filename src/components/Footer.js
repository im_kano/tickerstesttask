import React from 'react';
import {withRouter} from 'react-router-dom';

import facebookIco from '../../public/img/facebook.png';
import instagramIco from '../../public/img/instagram.png';
import twitterIco from '../../public/img/twitter.png';
import linkedinIco from '../../public/img/linkedin.png';


const Footer = props => (
    <div className='footer-container'>
        <div className='footer-container-logo'>
            <img src={facebookIco} alt=""/>
        </div>
        <div className='footer-container-logo'>
            <img src={instagramIco} alt=""/>
        </div>
        <div className='footer-container-logo'>
            <img src={twitterIco} alt=""/>
        </div>
        <div className='footer-container-logo'>
            <img src={linkedinIco} alt=""/>
        </div>
    </div>
);

export default withRouter(Footer);