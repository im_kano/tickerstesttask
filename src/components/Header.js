import React from 'react';
import {Link, withRouter} from "react-router-dom";

import logoCol from '../../public/img/rose.png';
import logoBw from '../../public/img/rose-bw.png';

const Header = props => {
    const headerColor = props.location.pathname === '/' ? '' : ' list';
    const headerActive = props.location.pathname === '/' ? '' : 'menu-active-second';
    const logo = props.location.pathname === '/' ? logoCol : logoBw;
    console.log(props.location.pathname);
    return (
        <div className={`header-container ${headerColor}`}>
            <div className='pos-rel'>
                <div className='logo'>
                    <Link to='/'>
                        <img src={logo}/>
                    </Link>
                </div>
                <div className={`menu-active ${headerActive}`}>

                </div>
                <nav>
                    <ul>
                        <li>
                            <Link className='menu-item' to="/">
                                Home
                            </Link>
                        </li>
                        <li>
                            <Link className='menu-item' to='/list'>
                                List
                            </Link>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    );
};

export default withRouter(Header);