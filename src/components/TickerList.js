import React from 'react';

const TickerList = props => {

    const {sortItemHandler, sorted, resetSort} = props; // destructuring props
    //map of the table header items so you won't need to give classname and onclick on each separately
    const tableHeaderItems = ['open', 'high', 'low', 'volume', 'close'].map((item, index) => (
        <th key ={index}
            className={sorted === item ? 'active-header' : ''}
            onClick={() => sortItemHandler(item)}
        >
            {item}
            </th>
    ));
//maping all tickers to make them a part of the html structure
    const tickerList = props.tickersItems.map((ticker, i) => {
        const {open, high, low, volume, close} = ticker; //destructuring ticker object keys
        return (<tr
            key={i}
            className={props.active === ticker ? 'active' : ''}
            onClick={() => props.tickerClickHandle(ticker)}
        >
            <td>{i+1}</td>
            <td>{open}</td>
            <td>{high}</td>
            <td>{low}</td>
            <td>{volume}</td>
            <td>{close}</td>
        </tr>);
    });
    return (
        <div className='ticker-list-wrapper'>
            <div className='table-wrapper'>
                <table>
                    <thead>
                    <tr>
                        <th>no</th>
                        {tableHeaderItems}
                    </tr>
                    </thead>
                    <tbody>
                    {tickerList}
                    </tbody>
                </table>
            </div>
            {/* conditionally rendered button if the table was sorted */}
            {sorted && <button
                className='sort-reset'
                onClick={resetSort}
            >
                Reset</button>}
            {/* loading animation while fetching data from the API */}
            <div className={props.isLoading && !props.tickersItems.length ? 'lds-dual-ring' : ''}>
            </div>
        </div>
    );
};
export default TickerList;