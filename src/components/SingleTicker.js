import React from 'react';

// Component that shows details of the clicked ticker in TickerList component
const SingleTicker = props => {
    const singleTickerDetails = Object.keys(props.active).map((key, ind) => {
        return (<div
            key={ind}
            className='single-ticker-wrapper-single-descr'>
            <h1>{key} </h1>
            <span>{props.active[key]}</span>
        </div>);
    });
    return (
        <div className='single-ticker-wrapper'>
            {singleTickerDetails}
        </div>

    );
};
export default SingleTicker;