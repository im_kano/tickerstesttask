import { combineReducers } from 'redux';
import tickers from '../modules/fetchTickers';

const reducers = {
    tickers,
};
export default combineReducers(reducers);