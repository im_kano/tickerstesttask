import React, {Component} from 'react';
import { connect } from 'react-redux';
import 'babel-polyfill';

import * as fromFetchTickers from '../modules/fetchTickers';
import TickerList from '../components/TickerList';
import SignleTicker from '../components/SingleTicker';

class List extends Component {
    constructor(props) {
        super(props);

    }
// name speaks for itself
    tickerClickHandle = (ticker) => {
        this.props.changeTicker(ticker);
    };
// name speaks for itself
    sortItemHandler = criteria => {
        this.props.sortItems(criteria);
    };
// initial fetching data from the API when the component is mounted
    componentDidMount(){
        this.props.fetchTickers();
    }

    render() {
        //destructuring props
        const {tickersItems, isLoading, active, sorted, resetSort} = this.props;
        return (
            <div className='list-container'>
                <TickerList
                    tickerClickHandle = {this.tickerClickHandle}
                    sortItemHandler = {this.sortItemHandler}
                    tickersItems = {tickersItems}
                    resetSort = {resetSort}
                    isLoading = {isLoading}
                    active = {active}
                    sorted = {sorted}
                />
                <SignleTicker
                    active = {active}
                />
            </div>
        );
    }

}

const mapStateToProps = state => ({
        tickersItems: state.tickers.items,
        isLoading: state.tickers.isLoading,
        active: state.tickers.active,
        sorted: state.tickers.sorted
    });

const mapDispatchToProps = dispatch => ({
        fetchTickers: () => dispatch(fromFetchTickers.tickersFetchData()),
        changeTicker: ticker => dispatch(fromFetchTickers.changeTicker(ticker)),
        sortItems: criteria => dispatch(fromFetchTickers.sortItems(criteria)),
        resetSort: () => dispatch(fromFetchTickers.resetSort()),
});

export default connect(mapStateToProps, mapDispatchToProps)(List);