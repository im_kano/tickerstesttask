import React, {Component} from 'react';
import { Route, Switch, HashRouter} from "react-router-dom";
// import { TransitionGroup, CSSTransition } from 'react-transition-group';

import Header from '../components/Header';
import Content from '../components/Content';
import List from './List';
import Footer from "../components/Footer";

import './App.css';

class App extends Component{
    constructor(props){
        super(props);

    }


    render() {
        return (
            <HashRouter>
                <div className='app-wrapper'>
                    <Header />
                    <Switch>
                        <Route
                            exact path ='/'
                            render={
                                () => <Content />
                            }
                        />
                        <Route path='/list' component={List}/>
                    </Switch>
                    <Footer />
                </div>
            </HashRouter>
        );
    }
}
export default App;